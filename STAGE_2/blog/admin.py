from django.contrib import admin
from .models import HealthTracker

admin.site.register(HealthTracker)
