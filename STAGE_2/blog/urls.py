from django.urls import path
from .views import HealthTrackerListView, HealthTrackerDetailView, HealthTrackerCreateView, HealthTrackerUpdateView, HealthTrackerDeleteView
from . import views

urlpatterns = [
    path('', HealthTrackerListView.as_view(), name='blog-home'),
    path('healthTracker/<int:pk>', HealthTrackerDetailView.as_view(), name='post-detail'),
    path('healthTracker/new/', HealthTrackerCreateView.as_view(), name='post-create'),
    path('healthTracker/<int:pk>/update', HealthTrackerUpdateView.as_view(), name='post-update'),
    path('healthTracker/<int:pk>/delete', HealthTrackerDeleteView.as_view(), name='post-delete'),
]