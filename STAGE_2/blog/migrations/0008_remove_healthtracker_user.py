# Generated by Django 2.2.1 on 2019-05-15 11:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_healthtracker_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='healthtracker',
            name='user',
        ),
    ]
