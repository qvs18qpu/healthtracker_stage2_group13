from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import HealthTracker


def home(request):
    context = {
        'posts': HealthTracker.objects.all()
    }
    return render(request, 'blog/home.html', context)


class HealthTrackerListView(ListView):
    model = HealthTracker
    template_name = 'blog/home.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'posts'
    

class HealthTrackerDetailView(DetailView):
    model = HealthTracker
    
class HealthTrackerCreateView(LoginRequiredMixin, CreateView):
    model = HealthTracker
    fields = ['exerciseTitle','exerciseduration', 'caloriesBurned', 'mealTitle', 'mealDescription', 'caloriesGain', 'goalTitle', 'goalDuration', 'goalDescription', 'calorieMax']
	
    def form_valid(self, form):
        form.instance.username = self.request.user
        return super().form_valid(form)


class HealthTrackerUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = HealthTracker
    fields = ['exerciseTitle','exerciseduration', 'caloriesBurned', 'mealTitle', 'mealDescription', 'caloriesGain', 'goalTitle', 'goalDuration', 'goalDescription', 'calorieMax']
	
    def form_valid(self, form):
        form.instance.username = self.request.user
        return super().form_valid(form)

    def test_func(self):
        healthTracker = self.get_object()
        if self.request.user == healthTracker.username:
            return True;
        return False

class HealthTrackerDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = HealthTracker
    success_url = '/'

    def test_func(self):
        healthTracker = self.get_object()
        if self.request.user == healthTracker.username:
            return True
        return False
		
def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})
