from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse


class HealthTracker(models.Model):

    exerciseTitle = models.CharField(max_length=100)
    exerciseduration = models.IntegerField()
    caloriesBurned = models.IntegerField()
    mealTitle = models.CharField(max_length=100)
    mealDescription = models.TextField()
    caloriesGain = models.IntegerField()
    goalTitle = models.CharField(max_length=100)
    goalDuration = models.IntegerField()
    goalDescription = models.TextField()
    calorieMax = models.IntegerField()
    username = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.exerciseTitle
		
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})

		
